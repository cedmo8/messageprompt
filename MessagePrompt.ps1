﻿[CmdletBinding()]
Param(
  [Parameter(Mandatory=$True,Position=1)]
   [string[]]$computerName,
	
   [Parameter(Mandatory=$True,Position=2)]
   [string]$message,

   [Parameter(Mandatory=$False,Position=3)]
   [string]$speak,

   [Parameter(Mandatory=$False,Position=4)]
   [string]$time = '3'

)

$messagelog = "\\rmlv02\GPO\Scripts\Logs\MessagePrompt\Messages.Log"
$errorlog = "\\rmlv02\GPO\Scripts\Logs\MessagePrompt\Errors.Log"

foreach ($computer in $computerName) {



        TRY {
                #Invoke-WmiMethod -Class Win32_Process -ComputerName $computer -Name Create -ArgumentList "C:\Windows\System32\msg.exe /time $time * $message" -ErrorAction Stop | Out-Null
                Invoke-Command -ComputerName $computer -ErrorAction Stop -ArgumentList $message, $time, $speak -ScriptBlock {
                
                    C:\Windows\System32\msg.exe /time $args[1] * $args[0]

                    IF ($args[2]) {

                        [Reflection.Assembly]::LoadWithPartialName('System.Speech') | Out-Null
                        $phrase =  $args[2]
                        $object = New-Object System.Speech.Synthesis.SpeechSynthesizer
                        $object.SelectVoiceByHints('Female')
                        $object.Speak($phrase)
                    }
                }
            }

        CATCH
            {
                $errortype = $_.Exception.GetType().FullName
                $output = "FAILED - Target: $computer - User Account: $([Environment]::UserDomainName)\$([Environment]::UserName) - Error: $_ ErrorType: $errortype - Message: $message"
                Write-Output $output
                [string]$(get-date)+' - '+$output | Out-File -Append "Microsoft.PowerShell.Core\FileSystem::$errorlog"
                continue
            }

                $output = "SUCCESS - Target: $computer - Message: $message"
                Write-Output $output
                [string]$(get-date)+' - '+$output | Out-File -Append "Microsoft.PowerShell.Core\FileSystem::$messagelog"

}


